/*
 *	sv (stream views), will input a http: stream using a url and port then disseminate as single and scable viewable
 *  or audible streams on a single local viewable screen
 *
 *  Copyright (C) 2016  Andy Laberge (andylaberge@linux.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	If you did not receive a copy (most probable) of the GNU General Public License
 *	along with this program; go see <http://www.gnu.org/licenses/>.
 */

#include "sv.h"
#include "ui_sv.h"
#include <QtNetwork/QNetworkInterface>





sv::sv(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::sv)
{

    ui->setupUi(this);
    helpBrowser();
/*
    QNetworkInterface eth0Ip;

    eth0Ip = QNetworkInterface::interfaceFromName("eth0");
    QList<QNetworkAddressEntry> entries = eth0Ip.addressEntries();
    if (!entries.isEmpty()) {
        QNetworkAddressEntry entry = entries.first();
       ui->textEdit->insertPlainText(entry.ip());
    }
*/

    QNetworkInterface *inter = new QNetworkInterface();
    QList<QHostAddress> list;
    list=inter->allAddresses();
    //QString str;
    ui->urllineEdit->clear();
    ui->urllineEdit->insert( list.at(2).toString());



}

QObject *parent;

QProcess *cmdlnVLM = new QProcess(parent);

//QString mksure = "Please make sure your http:// stream is running. Try pinging for an echo!";


sv::~sv()
{
    cmdlnVLM->kill();
    delete ui;
}





void sv::VLMconfigCall()
{
    // this part will call the built xxxx_vlm.config using vlc

    QString prog ="cvlc"; // using cvlc eliminates interface
    QStringList argu ;
    QString vlmconf ="--vlm-conf";

    QString vlmconf_path = QDir::homePath();

    vlmconf_path.append("/");
    vlmconf_path.append(ui->stream_out_ID_lineEdit->text());
    vlmconf_path.append("_vlm.conf");

    argu<<vlmconf<<vlmconf_path;


    cmdlnVLM->start(prog,argu);
}

void sv::createStreams()
{
    //this is where multiple instances of vlc are called to
    // view individual program instances on local network
    // with different ports

    int i;
    int cnt;
/*
    QString vlmconf_path = QDir::homePath();

    vlmconf_path.append("/");
    vlmconf_path.append(ui->stream_out_ID_lineEdit->text());
    vlmconf_path.append("_vlm.conf");
*/
  //  QString audio = "--no-volume-save";
    QString prog;
    QString nvd = "--no-video-deco";
    QString qeo = "--qt-embedded-open";
    QString nev = "--qt-minimal-view";
    QString nqst ="--no-qt-system-tray" ;
    QString rtplocal;
    QString marquee = "--qt-name-in-title";
    QString vt;
    QString vtshw = "--qt-name-in-title";
    QString tstring;
    QString wide = "-x";
    QString widenum = "160";
    QString high = "-y";
    QString highnum = "120";
    QString scale;
    QStringList argu;
    QStringList ffargu;
    QStringList list;
    QString tnum;
    QString tport;
    QString rtpPort;

    // develop loop counter by countin commas
    tstring=ui->programslineEdit->text();
    cnt=tstring.count(QRegExp(","));
    // add one for missing comma, add another cause loop starts at 1
    cnt+=2;
    // while here make an array
    list=tstring.split(",");


    for(i=1;i<cnt;i++)
    {
        scale="--zoom=";
        scale.append(QString::number(ui->scale_doubleSpinBox->value()));
        if(ui->udp_checkBox->isChecked()==true)
         {
            rtplocal="udp://@127.0.0.1:";
          }
        else
        {
        rtplocal="rtp://127.0.0.1:";
        }
        argu.clear();
        rtpPort.clear();
        vt="--video-title=";
        tnum=QString::number(i);
        tport=ui->portlineEdit->text();
        rtpPort= QString::number(ui->outPort_spinBox->value());

        if(i<10)
        rtpPort.append("0");
        rtpPort.append(tnum);
        rtplocal.append(rtpPort);
        vt.append(ui->stream_out_ID_lineEdit->text());
        vt.append(" Prg. ");
        vt.append(list.at(i-1));


        argu<<scale<<nqst<<qeo<<vt<<vtshw<<marquee<<nvd<<nev<<rtplocal;
       // ffargu<<wide<<widenum<<high<<highnum<<nqst<<qeo<<nev<<nvd<<vt<<rtplocal;
         // argu<<rtplocal;

           if(ui->shw_all_checkBox->isChecked()==true)
           {
               prog="vlc";
               QProcess *vlcrtp = new QProcess(this);
                 vlcrtp->start(prog,argu);

           }
           else
           {
               prog="cvlc";
               QProcess *vlcrtp = new QProcess(this);
               vlcrtp->start(prog,argu);
           }
    }

        }







void sv::editVLMconfig()
{
// this part will take the input information and create a VLM
// config



    QString tstring;
    QString tport;
    QString tnum;
 //   QString rtpSetup="del all\n\n";
    QString rtpPort;
    QString rtpRepeat = "dst=rtp{mux=ts,ttl=12,dst=127.0.0.1,port=";
    QString rtpProg = "},select= program=";
    QString rtpProgA;
    QString rtpSetup;
    QStringList list;
    QString homepath = QDir::homePath();

    //homepath.append("/sv2");

    int cnt;
    int i;


    // let's clear edit screen
    ui->textEdit->clear();
    // develop loop counter by countin commas
    tstring=ui->programslineEdit->text();
    cnt=tstring.count(QRegExp(","));
    // add one for missing comma, add another cause loop starts at 1
    cnt+=2;
    // while here make an array
    list=tstring.split(",");


   // ui->textEdit->insertPlainText(rtpPort+"\n\n");


    // enter "del all" may remove this later
    ui->textEdit->insertPlainText(rtpSetup);
    rtpSetup.clear();

    //write first line of VLM configuration "new xxxx broadcast enabled"
    rtpSetup="new "+ui->stream_out_ID_lineEdit->text()+" broadcast enabled\n";
    ui->textEdit->insertPlainText(rtpSetup);
    rtpSetup.clear();

    //next line is input
    rtpSetup="setup "+ui->stream_out_ID_lineEdit->text()+" input http://";
    rtpSetup.append(ui->urllineEdit->text());
    rtpSetup.append(":"+ui->portlineEdit->text()+"\n");
    ui->textEdit->insertPlainText(rtpSetup);
    rtpSetup.clear();

    //now the programs option
    rtpSetup="setup "+ui->stream_out_ID_lineEdit->text()+" option programs="+ui->programslineEdit->text()+"\n\n";
    ui->textEdit->insertPlainText(rtpSetup);
    rtpSetup.clear();

    //now the output .. output for some reason or another needs single or double quotes to enclose it
    rtpSetup="setup "+ui->stream_out_ID_lineEdit->text()+" output '#duplicate{";
    // now we append multiple times
    if(ui->udp_checkBox->isChecked()==true)
    {
        rtpRepeat="dst=udp{mux=ts,ttl=12,dst=127.0.0.1:";
    }
    else rtpRepeat="dst=rtp{mux=ts,ttl=12,dst=127.0.0.1,port=";

    for(i=1;i<cnt;i++)
    {
        rtpSetup.append(rtpRepeat);
        tnum=QString::number(i);
      //  rtpPort.clear();
        tport=ui->portlineEdit->text();
        rtpPort= QString::number(ui->outPort_spinBox->value());
        if(i<10)
        rtpPort.append("0");
        rtpPort.append(tnum);
        rtpSetup.append(rtpPort);
        rtpProgA=rtpProg;
        rtpProgA.append(list.at(i-1));
        rtpSetup.append(rtpProgA+",");

    }

    rtpSetup.append("}'\n\n");
    ui->textEdit->insertPlainText(rtpSetup);
    rtpSetup.clear();

    // after "new" and "setup" comes "control"

    rtpSetup="control "+ui->stream_out_ID_lineEdit->text()+" play \n";
    ui->textEdit->insertPlainText(rtpSetup);
    rtpSetup.clear();

    homepath.append("/");
    homepath.append(ui->stream_out_ID_lineEdit->text());
    homepath.append("_vlm.conf");


    QFile file(homepath);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
             return ;
    QTextStream out(&file);

    out<<ui->textEdit->toPlainText();

    statusBar()->showMessage(tr("File - %1 - Saved")
            .arg(homepath));

    return;


}

void sv::helpBrowser()
{
    ui->textEdit->clear();

    QString mksure = "Step1: Please make sure your host http:// stream is running. This is easy to forget!! Try pinging for an echo if unsure.\n\n";
    ui->textEdit->insertPlainText(mksure);

    mksure = "Step2: URL box contains host address.. just the address no Port. \n\n";
    ui->textEdit->insertPlainText(mksure);

    mksure = "Step3: Port box is where host port must be correctly entered.\n\n";
    ui->textEdit->insertPlainText(mksure);

    mksure = "Step4: Out Port Start must be unique to each different sv instance you have running at the same time. "
             "If this is the first of instance of sv then 30 is ok. If this is the second instance of sv use 31, third instance use 32 and so on.\n\n";
    ui->textEdit->insertPlainText(mksure);

    mksure = "Step5: Stream ID has to be unique to each sv instance running and 1 word only of alphanumerics with no spaces or special characters.\n\n";
    ui->textEdit->insertPlainText(mksure);

    mksure = "Step6: Programs box is where program numbers must be entered with only comma delimiters last one has no comma. Also they must correlate to your host stream.\nEx-> 13,4,57\nEx-> 10\n\n";
    ui->textEdit->insertPlainText(mksure);

    mksure = "Step7: Make sure Create VLM conf button is active; then press GO. A VLM configure file will be created and posted.\n\n";
    ui->textEdit->insertPlainText(mksure);

    mksure = "Step8: After seeing your VLM conf file created, click the Create Streams button and press GO again!\n\n";
    ui->textEdit->insertPlainText(mksure);

    mksure = "PLEASE scroll to Top and start with Step1:\n";
    ui->textEdit->insertPlainText(mksure);
}

void sv::on_pushButton_clicked()
{
    // check radio buttons and do
    if(ui->createStreamsRadioButton->isChecked()==true)
    {
        VLMconfigCall();
        createStreams();
    }
    else if(ui->editRadioButton->isChecked()==true)
      {  editVLMconfig();
        // writeVLMcommandLines();
      }

 }

void sv::on_pushButton_2_clicked()
{

    helpBrowser();

    return;
}

