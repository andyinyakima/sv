/********************************************************************************
** Form generated from reading UI file 'sv.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SV_H
#define UI_SV_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_sv
{
public:
    QWidget *centralWidget;
    QGroupBox *groupBox;
    QRadioButton *createStreamsRadioButton;
    QRadioButton *editRadioButton;
    QPushButton *pushButton;
    QDoubleSpinBox *scale_doubleSpinBox;
    QLabel *label_6;
    QCheckBox *udp_checkBox;
    QCheckBox *shw_all_checkBox;
    QTextEdit *textEdit;
    QGroupBox *input_groupBox;
    QLineEdit *portlineEdit;
    QLineEdit *urllineEdit;
    QLabel *label_2;
    QLabel *label;
    QSpinBox *outPort_spinBox;
    QLabel *label_5;
    QPushButton *pushButton_2;
    QLineEdit *stream_out_ID_lineEdit;
    QLabel *label_4;
    QLineEdit *programslineEdit;
    QLabel *label_3;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *sv)
    {
        if (sv->objectName().isEmpty())
            sv->setObjectName(QStringLiteral("sv"));
        sv->resize(474, 438);
        centralWidget = new QWidget(sv);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(10, 0, 201, 171));
        createStreamsRadioButton = new QRadioButton(groupBox);
        createStreamsRadioButton->setObjectName(QStringLiteral("createStreamsRadioButton"));
        createStreamsRadioButton->setGeometry(QRect(0, 40, 121, 22));
        createStreamsRadioButton->setChecked(false);
        editRadioButton = new QRadioButton(groupBox);
        editRadioButton->setObjectName(QStringLiteral("editRadioButton"));
        editRadioButton->setGeometry(QRect(0, 20, 131, 22));
        editRadioButton->setChecked(true);
        pushButton = new QPushButton(groupBox);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(0, 90, 31, 27));
        QPalette palette;
        QBrush brush(QColor(112, 174, 121, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Highlight, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Highlight, brush);
        QBrush brush1(QColor(174, 174, 174, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Disabled, QPalette::Highlight, brush1);
        pushButton->setPalette(palette);
        scale_doubleSpinBox = new QDoubleSpinBox(groupBox);
        scale_doubleSpinBox->setObjectName(QStringLiteral("scale_doubleSpinBox"));
        scale_doubleSpinBox->setGeometry(QRect(60, 90, 62, 27));
        scale_doubleSpinBox->setMinimum(0.1);
        scale_doubleSpinBox->setMaximum(1);
        scale_doubleSpinBox->setSingleStep(0.05);
        scale_doubleSpinBox->setValue(0.2);
        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(140, 100, 59, 17));
        udp_checkBox = new QCheckBox(groupBox);
        udp_checkBox->setObjectName(QStringLiteral("udp_checkBox"));
        udp_checkBox->setGeometry(QRect(0, 140, 61, 22));
        shw_all_checkBox = new QCheckBox(groupBox);
        shw_all_checkBox->setObjectName(QStringLiteral("shw_all_checkBox"));
        shw_all_checkBox->setGeometry(QRect(0, 120, 81, 22));
        shw_all_checkBox->setChecked(true);
        textEdit = new QTextEdit(centralWidget);
        textEdit->setObjectName(QStringLiteral("textEdit"));
        textEdit->setGeometry(QRect(10, 180, 441, 221));
        input_groupBox = new QGroupBox(centralWidget);
        input_groupBox->setObjectName(QStringLiteral("input_groupBox"));
        input_groupBox->setGeometry(QRect(230, 0, 241, 181));
        portlineEdit = new QLineEdit(input_groupBox);
        portlineEdit->setObjectName(QStringLiteral("portlineEdit"));
        portlineEdit->setGeometry(QRect(0, 50, 131, 27));
        urllineEdit = new QLineEdit(input_groupBox);
        urllineEdit->setObjectName(QStringLiteral("urllineEdit"));
        urllineEdit->setGeometry(QRect(0, 20, 131, 27));
#ifndef QT_NO_TOOLTIP
        urllineEdit->setToolTip(QStringLiteral("<html><head/><body><p>This is the IP address of the stream; make sure it is the same as the host stream coming in.</p></body></html>"));
#endif // QT_NO_TOOLTIP
        urllineEdit->setToolTipDuration(-1);
        label_2 = new QLabel(input_groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(140, 60, 31, 17));
        label = new QLabel(input_groupBox);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(140, 30, 31, 17));
        outPort_spinBox = new QSpinBox(input_groupBox);
        outPort_spinBox->setObjectName(QStringLiteral("outPort_spinBox"));
        outPort_spinBox->setGeometry(QRect(0, 80, 44, 27));
        outPort_spinBox->setMinimum(30);
        outPort_spinBox->setMaximum(40);
        label_5 = new QLabel(input_groupBox);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(50, 90, 91, 17));
        pushButton_2 = new QPushButton(input_groupBox);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(190, 70, 41, 27));
        stream_out_ID_lineEdit = new QLineEdit(input_groupBox);
        stream_out_ID_lineEdit->setObjectName(QStringLiteral("stream_out_ID_lineEdit"));
        stream_out_ID_lineEdit->setGeometry(QRect(0, 110, 131, 27));
        label_4 = new QLabel(input_groupBox);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(140, 120, 61, 17));
        programslineEdit = new QLineEdit(input_groupBox);
        programslineEdit->setObjectName(QStringLiteral("programslineEdit"));
        programslineEdit->setGeometry(QRect(0, 140, 131, 27));
        label_3 = new QLabel(input_groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(140, 150, 61, 20));
        sv->setCentralWidget(centralWidget);
        mainToolBar = new QToolBar(sv);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        sv->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(sv);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        sv->setStatusBar(statusBar);
        QWidget::setTabOrder(urllineEdit, portlineEdit);
        QWidget::setTabOrder(portlineEdit, outPort_spinBox);
        QWidget::setTabOrder(outPort_spinBox, stream_out_ID_lineEdit);
        QWidget::setTabOrder(stream_out_ID_lineEdit, programslineEdit);
        QWidget::setTabOrder(programslineEdit, editRadioButton);
        QWidget::setTabOrder(editRadioButton, createStreamsRadioButton);
        QWidget::setTabOrder(createStreamsRadioButton, scale_doubleSpinBox);
        QWidget::setTabOrder(scale_doubleSpinBox, pushButton);
        QWidget::setTabOrder(pushButton, shw_all_checkBox);
        QWidget::setTabOrder(shw_all_checkBox, udp_checkBox);
        QWidget::setTabOrder(udp_checkBox, pushButton_2);
        QWidget::setTabOrder(pushButton_2, textEdit);

        retranslateUi(sv);

        QMetaObject::connectSlotsByName(sv);
    } // setupUi

    void retranslateUi(QMainWindow *sv)
    {
        sv->setWindowTitle(QApplication::translate("sv", "Stream Views", 0));
        groupBox->setTitle(QApplication::translate("sv", "Function", 0));
        createStreamsRadioButton->setText(QApplication::translate("sv", "Play Streams", 0));
        editRadioButton->setText(QApplication::translate("sv", "Create VLM conf", 0));
        pushButton->setText(QApplication::translate("sv", "Go", 0));
#ifndef QT_NO_TOOLTIP
        scale_doubleSpinBox->setToolTip(QApplication::translate("sv", "<html><head/><body><p>Resize your stream views. SD views will be smaller then HD views. </p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        label_6->setText(QApplication::translate("sv", "Scale", 0));
        udp_checkBox->setText(QApplication::translate("sv", "UDP", 0));
        shw_all_checkBox->setText(QApplication::translate("sv", "Show All", 0));
        textEdit->setHtml(QApplication::translate("sv", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Noto Sans'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>", 0));
        input_groupBox->setTitle(QApplication::translate("sv", "Inputs", 0));
#ifndef QT_NO_TOOLTIP
        portlineEdit->setToolTip(QApplication::translate("sv", "<html><head/><body><p>This must be the port number of the host stream coming in.</p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        portlineEdit->setText(QApplication::translate("sv", "1230", 0));
        urllineEdit->setInputMask(QString());
        urllineEdit->setText(QApplication::translate("sv", "192.168.1.5", 0));
        label_2->setText(QApplication::translate("sv", "Port", 0));
        label->setText(QApplication::translate("sv", "URL", 0));
#ifndef QT_NO_TOOLTIP
        outPort_spinBox->setToolTip(QApplication::translate("sv", "<html><head/><body><p>This is the start of the rtp port 30xx; the xx part is added automatically. If you call up another instance of &quot;sv&quot;  increment this value to reflect the number instances of &quot;sv&quot;.</p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        label_5->setText(QApplication::translate("sv", "Out Port Start", 0));
        pushButton_2->setText(QApplication::translate("sv", "Help", 0));
#ifndef QT_NO_TOOLTIP
        stream_out_ID_lineEdit->setToolTip(QApplication::translate("sv", "<html><head/><body><p>This has to be one word only of alphanumeric input. NO SPACES or Special Characters. </p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        stream_out_ID_lineEdit->setText(QApplication::translate("sv", "test", 0));
        label_4->setText(QApplication::translate("sv", "Stream ID", 0));
#ifndef QT_NO_TOOLTIP
        programslineEdit->setToolTip(QApplication::translate("sv", "<html><head/><body><p>You must know the Program numbers of the services you want to stream; a comma is the only delimiter that is understood. The numbers need to correlate to host stream program numbers.</p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        programslineEdit->setText(QApplication::translate("sv", "3", 0));
        programslineEdit->setPlaceholderText(QString());
        label_3->setText(QApplication::translate("sv", "Programs", 0));
    } // retranslateUi

};

namespace Ui {
    class sv: public Ui_sv {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SV_H
