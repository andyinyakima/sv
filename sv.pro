#-------------------------------------------------
#
# Project created by QtCreator 2016-04-26T09:53:06
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = sv
TEMPLATE = app


SOURCES += main.cpp\
        sv.cpp

HEADERS  += sv.h

FORMS    += sv.ui
