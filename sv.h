#ifndef SV_H
#define SV_H

#include <QApplication>
#include <QMainWindow>
#include <QProcess>
#include <QDir>
#include <QFileDialog>
#include <QMessageBox>
#include <QTextStream>
#include <QFont>
#include <QtNetwork/QNetworkInterface>


namespace Ui {
class sv;
}

class sv : public QMainWindow
{
    Q_OBJECT

public:
    explicit sv(QWidget *parent = 0);
    ~sv();

private slots:

    void VLMconfigCall();

    void createStreams();

    void editVLMconfig();

    void helpBrowser();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();


private:
    Ui::sv *ui;
};

#endif // SV_H
